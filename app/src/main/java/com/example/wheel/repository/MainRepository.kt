package com.example.wheel.repository


import com.example.wheel.network.ApiService


class MainRepository(private val apiService: ApiService) {
    suspend fun getWheelRpm(min: String, max: String): String {
        val result = apiService.fetchWheelRpm(min, max).first().random
        return result
    }
}