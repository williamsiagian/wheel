package com.example.wheel.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.wheel.repository.MainRepository
import com.example.wheel.network.ApiClient
import com.example.wheel.utils.ScreenState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class WheelRpmViewModel(private val repository: MainRepository = MainRepository(ApiClient.apiService)) :
    ViewModel() {
    private var _wheelRpmLiveData = MutableLiveData<ScreenState<String>>()
    val wheelRpmLiveData: LiveData<ScreenState<String>>
        get() = _wheelRpmLiveData


    fun fetchViewModelWheelRpm() {

        viewModelScope.launch(Dispatchers.IO) {

            _wheelRpmLiveData.postValue(ScreenState.Loading(null))
            try {
                val rpmValue = repository.getWheelRpm("0", "100")
                _wheelRpmLiveData.postValue(ScreenState.Success(rpmValue))

            } catch (e: Exception) {
                _wheelRpmLiveData.postValue(ScreenState.Error(e.message.toString(), null))

            }
        }
    }


}