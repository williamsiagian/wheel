package com.example.wheel

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.wheel.utils.ScreenState
import com.example.wheel.viewmodel.WheelRpmViewModel
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private val viewModel: WheelRpmViewModel by lazy {
        ViewModelProvider(this)[WheelRpmViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tvRpm = findViewById<TextView>(R.id.tvRpm)
        val btnRpm = findViewById<Button>(R.id.btnRpm)

        tvRpm.text = getString(R.string.initial_rpm)

        btnRpm.setBackgroundColor(resources.getColor(R.color.button_update_background))

        btnRpm.setOnClickListener {

            getWheelRpmData()

        }
    }

    private fun setWheelRpmSpeed(rpmValue: Long) {

        val ivRpm = findViewById<ImageView>(R.id.ivRpm)

        val rotateAnimation = RotateAnimation(
            0F, 360F, RotateAnimation.RELATIVE_TO_SELF,
            .5f, RotateAnimation.RELATIVE_TO_SELF, .5f
        )

        val calculateRpm = (100 / rpmValue) * 500
        rotateAnimation.duration = calculateRpm
        rotateAnimation.repeatCount = 999999
        ivRpm.startAnimation(rotateAnimation)
    }

    private fun getWheelRpmData() {

        val tvRpm = findViewById<TextView>(R.id.tvRpm)
        val btnRpm = findViewById<Button>(R.id.btnRpm)
        val snackBarLayout = findViewById<LinearLayout>(R.id.mainLinearLayout)

        viewModel.fetchViewModelWheelRpm()

        viewModel.wheelRpmLiveData.observe(this, { state ->

            when (state) {
                is ScreenState.Loading -> {
                    showSnackBar(snackBarLayout, getString(R.string.update_rpm))
                    btnRpm.isEnabled = false

                }

                is ScreenState.Success -> {

                    if (state.data != null) {
                        val rpmString = "${state.data} RPM"
                        tvRpm.text = rpmString
                        setWheelRpmSpeed(state.data.toLong())

                    }
                    btnRpm.isEnabled = true

                    showSnackBar(snackBarLayout, getString(R.string.update_rpm_success))
                }

                is ScreenState.Error -> {
                    showSnackBar(snackBarLayout, getString(R.string.update_rpm_failed))

                    btnRpm.isEnabled = true

                }
            }
        })

    }

    private fun showSnackBar(view: View, text: String) {

        val generatedSnackBar = Snackbar.make(
            view, text,
            Snackbar.LENGTH_SHORT
        )
        generatedSnackBar.setActionTextColor(Color.BLUE)
        val viewSnackBar = generatedSnackBar.view
        viewSnackBar.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_background))
        val textView =
            viewSnackBar.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(this, R.color.snack_bar_text))
        textView.textSize = 18f
        textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        generatedSnackBar.show()
    }
}