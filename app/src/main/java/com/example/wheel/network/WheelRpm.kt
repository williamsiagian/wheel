package com.example.wheel.network

import com.squareup.moshi.Json

data class WheelRpm(
    @Json(name = "random")
    val random: String
)


